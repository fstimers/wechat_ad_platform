# 简介
基于selenium和thinter，在微信广告平台自动创建不同类型的素材广告

# 安装步骤
1. 使用pip install -r requirements.txt安装依赖
2. 打包成exe文件参考 [Python项目打包成exe文件](https://blog.csdn.net/lyq19870515/article/details/85620730)
3. 下载谷歌驱动，选择和电脑上谷歌版本号一致的即可:[谷歌驱动](https://chromedriver.storage.googleapis.com/index.html)
# 使用
1. 分别在优雅横板大图、公众号文章中部、公众号文章底部、小程序banner广告、展示Banner图片目录下配置素材图片
2. 在dist中找到打包后exe文件，执行该文件
3. 第一次执行，选择登陆方式，扫码登陆后即可选择公众号执行创建对应的广告